#!/bin/bash
#-------------------------------------------------------------------------------
# install_ec2-api-tools.sh
#-------------------------------------------------------------------------------
# Copyright 2011 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/ec2-api-tools.zip http://ec2-downloads.s3.amazonaws.com/ec2-api-tools.zip
rm -fR /tmp/aws/ec2-api-tools-*
unzip -d /tmp/aws /tmp/aws/ec2-api-tools.zip
rm -fR /opt/aws/ec2-api-tools
mv /tmp/aws/ec2-api-tools-* /opt/aws/ec2-api-tools
rm -f /tmp/aws/ec2-api-tools.zip
cat <<'EOF'>/etc/profile.d/ec2-api-tools.sh
export EC2_HOME=/opt/aws/ec2-api-tools
export PATH=$PATH:$EC2_HOME/bin
EOF
