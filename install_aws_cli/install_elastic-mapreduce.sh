#!/bin/bash

mkdir -p /tmp/aws

mkdir -p /opt/aws

curl --silent \
--output /tmp/aws/elastic-mapreduce-ruby.zip \
http://elasticmapreduce.s3.amazonaws.com/elastic-mapreduce-ruby.zip

rm -fR /tmp/aws/elastic-mapreduce-ruby

mkdir -p /tmp/aws/elastic-mapreduce-ruby
unzip -d /tmp/aws/elastic-mapreduce-ruby /tmp/aws/elastic-mapreduce-ruby.zip

rm -fR /opt/aws/elastic-mapreduce-ruby

mv /tmp/aws/elastic-mapreduce-ruby /opt/aws/elastic-mapreduce-ruby

rm -f /tmp/aws/elastic-mapreduce-ruby.zip

cat <<'EOF'>/etc/profile.d/elastic-mapreduce-ruby.sh
export AWS_ELASTIC_MAPREDUCE_HOME=/opt/aws/elastic-mapreduce-ruby
export PATH=$PATH:$AWS_ELASTIC_MAPREDUCE_HOME
EOF
