#!/bin/bash
#-------------------------------------------------------------------------------
# install_elb.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/ElasticLoadBalancing.zip http://ec2-downloads.s3.amazonaws.com/ElasticLoadBalancing.zip
rm -fR /tmp/aws/ElasticLoadBalancing-*
unzip -d /tmp/aws /tmp/aws/ElasticLoadBalancing.zip
rm -fR /opt/aws/ElasticLoadBalancing
mv /tmp/aws/ElasticLoadBalancing-* /opt/aws/ElasticLoadBalancing
rm -f /tmp/aws/ElasticLoadBalancing.zip
cat <<'EOF'>/etc/profile.d/ElasticLoadBalancing.sh
export AWS_ELB_HOME=/opt/aws/ElasticLoadBalancing
export PATH=$PATH:$AWS_ELB_HOME/bin
EOF

