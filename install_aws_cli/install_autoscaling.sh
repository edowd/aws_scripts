#!/bin/bash
#-------------------------------------------------------------------------------
# install_autoscaling.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/AutoScaling.zip http://ec2-downloads.s3.amazonaws.com/AutoScaling-2011-01-01.zip
rm -fR /tmp/aws/AutoScaling-*
unzip -d /tmp/aws /tmp/aws/AutoScaling.zip
rm -fR /opt/aws/AutoScaling
mv /tmp/aws/AutoScaling-* /opt/aws/AutoScaling
rm -f /tmp/aws/AutoScaling.zip
cat <<'EOF'>/etc/profile.d/AutoScaling.sh
export AWS_AUTO_SCALING_HOME=/opt/aws/AutoScaling
export PATH=$PATH:$AWS_AUTO_SCALING_HOME/bin
EOF
