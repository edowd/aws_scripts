#!/bin/bash
#-------------------------------------------------------------------------------
# install_cli53.sh
#-------------------------------------------------------------------------------
# Copyright 2014 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

SETUPTOOLS=`which easy_install`
if [[ $? != 0 ]]; then
    curl https://bootstrap.pypa.io/ez_setup.py -o - | python
fi

git clone https://github.com/barnybug/cli53.git
cd cli53
sudo python setup.py install

