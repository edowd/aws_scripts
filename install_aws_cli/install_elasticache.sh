#!/bin/bash
#-------------------------------------------------------------------------------
# install_elasticache.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/AmazonElastiCacheCli.zip https://s3.amazonaws.com/elasticache-downloads/AmazonElastiCacheCli-latest.zip
rm -fR /tmp/aws/AmazonElastiCacheCli-*
unzip -d /tmp/aws /tmp/aws/AmazonElastiCacheCli.zip
rm -fR /opt/aws/AmazonElastiCacheCli
mv /tmp/aws/AmazonElastiCacheCli-* /opt/aws/AmazonElastiCacheCli
rm -f /tmp/aws/AmazonElastiCacheCli.zip
cat <<'EOF'>/etc/profile.d/AmazonElastiCacheCli.sh
export AWS_ELASTICACHE_HOME=/opt/aws/AmazonElastiCacheCli
export PATH=$PATH:$AWS_ELASTICACHE_HOME/bin
EOF

