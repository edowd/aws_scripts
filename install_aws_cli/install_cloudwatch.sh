#!/bin/bash
#-------------------------------------------------------------------------------
# install_cloudwatch.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/CloudWatch.zip http://ec2-downloads.s3.amazonaws.com/CloudWatch-2010-08-01.zip
rm -fR /tmp/aws/CloudWatch-*
unzip -d /tmp/aws /tmp/aws/CloudWatch.zip
rm -fR /opt/aws/CloudWatch
mv /tmp/aws/CloudWatch-* /opt/aws/CloudWatch
rm -f /tmp/aws/CloudWatch.zip
cat <<'EOF'>/etc/profile.d/CloudWatch.sh
export AWS_CLOUDWATCH_HOME=/opt/aws/CloudWatch
export PATH=$PATH:$AWS_CLOUDWATCH_HOME/bin
EOF

