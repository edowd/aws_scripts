#!/bin/bash
#-------------------------------------------------------------------------------
# install_s3curl.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /usr/local/share
mkdir -p /usr/local/bin
curl --silent --output /tmp/aws/s3-curl.zip http://s3.amazonaws.com/doc/s3-example-code/s3-curl.zip
unzip -d /tmp/aws /tmp/aws/s3-curl.zip
chmod 755 /tmp/aws/s3-curl/s3curl.pl
rm -fR /usr/local/share/s3-curl
mv /tmp/aws/s3-curl /usr/local/share
mv /usr/local/share/s3-curl/s3curl.pl /usr/local/bin
rm -fR /tmp/aws/s3-curl*

