#!/bin/bash
#-------------------------------------------------------------------------------
# install_iamcli.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------
mkdir -p /tmp/aws
mkdir -p /opt/aws
curl --silent --output /tmp/aws/IAMCli.zip http://awsiammedia.s3.amazonaws.com/public/tools/cli/latest/IAMCli.zip
rm -fR /tmp/aws/IAMCli-*
unzip -d /tmp/aws /tmp/aws/IAMCli.zip
rm -fR /opt/aws/IAMCli
mv /tmp/aws/IAMCli-* /opt/aws/IAMCli
rm -f /tmp/aws/IAMCli.zip
cat <<'EOF'>/etc/profile.d/IAMCli.sh
export AWS_IAM_HOME=/opt/aws/IAMCli
export PATH=$PATH:$AWS_IAM_HOME/bin
EOF

