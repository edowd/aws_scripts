#!/bin/bash

#-------------------------------------------------------------------------------
# amazon-key-pair.sh
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

name=$1
region=$2

if [[ $name == "" ]]; then
    echo "Name not set"
    exit 1
fi

if [[ $region == "" ]]; then
    region="us-east-1"
fi

ec2-add-keypair --region $region $name | sed 1d > $name.pem

